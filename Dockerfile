FROM python:3.10
LABEL authors="Pavel"
COPY . weather_project
CMD ["python", "/manage.py", "runserver"]
