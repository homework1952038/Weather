# Weather
To whomever is reading this have a wonderful day.


## Description:
I created a web-based tool that allows users to access current weather data and 7-day weather forecasts for cities around the world. I implemented all of the task required from me such as:  
 Show current weather in my location.  
 Search and show the weather in other cities.  
 User Authentication.  
 Weather Forecast(show the weather 7 days ahead).  

## Tools and Technologies Used:
Language: Python v.3.10  
Framework: Django REST framework  
Database: PostgreSQL 
API: OpenWeatherMap API  
Additionlly: ChatGPT to generate CSS, CodiumAI to generate unit tests.

## Screenshots:
### The "home screen" located on http://127.0.0.1:8000/api/weather/current/ , shows the weather in current location (by default Chisinau)
![name-of-you-image](https://gitlab.com/homework1952038/Weather/-/raw/main/screenshots/HW1.png?ref_type=heads)
In case you want to use the search function it will send you to the login page.
```
@login_required(login_url="login/")
```
### The login page:
![name-of-you-image](https://gitlab.com/homework1952038/Weather/-/raw/main/screenshots/HW2.png?ref_type=heads)
### The registration page (sorry for it being so scuffed):
![name-of-you-image](https://gitlab.com/homework1952038/Weather/-/raw/main/screenshots/HW3.png?ref_type=heads)
### In case you left the search space blank:
![name-of-you-image](https://gitlab.com/homework1952038/Weather/-/raw/main/screenshots/HW4.png?ref_type=heads)  
If you search for a non existing city you will get a error 404 and a message "Weather data not available for the specified city".
### A searched city:
![name-of-you-image](https://gitlab.com/homework1952038/Weather/-/raw/main/screenshots/HW5.png?ref_type=heads)
### Database screenshot with a created user: 
![name-of-you-image](https://gitlab.com/homework1952038/Weather/-/raw/main/screenshots/HW6.png?ref_type=heads)

## Project Launch
```
python -m venv env  
env\Scripts\activate  
"CD to project location" ex. cd weather_project
```
Install any project dependencies using pip or other package managers:
```
RUN pip install -r requirements.txt
```
Create Migrations and Apply Them: 
```
python manage.py makemigrations
python manage.py migrate
```
Start the project 
```
python manage.py runserver
```
## Conclusion:
Started working on the project on Tuesday the day after I returned from the interview, on the first day using some internet guides I managed to make the rudimentary site which communicated with the openweather API and show the information on a specified city.  
On the second day I created the endpoints "current" which shows the weather in chisinau and "search" which shows the weather of a searched city. In the same day I configured the connection with postgre database and created an authentication system.  
On the third day which is today corrected some problems with authentication and generated unit  test, dockerized the project(which I still think doesnt work correctly but I already took too much time with the homework) and done a general check of the project.   
It is not the best looking project but overall it is user friendly atleast, all of the specified requirements are done and some more. 