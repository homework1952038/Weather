from rest_framework import serializers

class WeatherSerializer(serializers.Serializer):
    city = serializers.CharField()
    temperature = serializers.DecimalField(max_digits=5, decimal_places=2)
    description = serializers.CharField()
    icon = serializers.CharField()