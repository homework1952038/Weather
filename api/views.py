import datetime
import requests
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import CustomUser
from .serializers import WeatherSerializer

API_KEY = '1be6324f259e2d5ad5e3f216c7627890'
CURRENT_WEATHER_URL = 'https://api.openweathermap.org/data/2.5/weather?q={}&appid={}'
FORECAST_URL = 'https://api.openweathermap.org/data/2.5/onecall?lat={}&lon={}&exclude=minutely,hourly,alerts&appid={}'


# Forms for registration and login
class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = CustomUser
        fields = ('username', 'password1', 'password2')


class CustomAuthenticationForm(AuthenticationForm):
    class Meta:
        model = CustomUser
        fields = ('username', 'password1', 'password2')


# Get the weather for the specified city
@api_view(['GET'])
def current_weather(request):
    city = 'Chisinau'  # Default city to display
    response = requests.get(CURRENT_WEATHER_URL.format(city, API_KEY)).json()

    if response.get('cod') == 200:
        lat, lon = response['coord']['lat'], response['coord']['lon']
        forecast_response = requests.get(FORECAST_URL.format(lat, lon, API_KEY)).json()

        weather_data = {
            'city': city,
            'temperature': round(response['main']['temp'] - 273.15, 2),
            'description': response['weather'][0]['description'],
            'icon': response['weather'][0]['icon'],
        }

        serializer = WeatherSerializer(weather_data)

        # Slice the data to get 7 days
        forecast_data = forecast_response['daily'][:7]

        # Extract descriptions and icons and reduce the temperature from kelvin to celsius
        daily_forecasts = []
        for daily in forecast_data:
            day_timestamp = daily['dt']
            weekday = datetime.datetime.fromtimestamp(day_timestamp).strftime('%A')
            daily_forecasts.append({
                'day': weekday,
                'min_temp': round(daily['temp']['min'] - 273.15, 2),
                'max_temp': round(daily['temp']['max'] - 273.15, 2),
                'description': daily['weather'][0]['description'],
                'icon': daily['weather'][0]['icon'],
            })

        return render(request, 'api/weather.html',
                      {'weather_data1': serializer.data, 'daily_forecasts': daily_forecasts})
    else:
        # Some error handling
        return Response({"message": "Weather data not available for Chisinau"}, status=status.HTTP_404_NOT_FOUND)


# A smart person wouldn't duplicate his code... but I am not a smart person.
@api_view(['GET'])
@login_required(login_url="login/")
def search_weather(request):
    city = request.query_params.get('city')

    response = requests.get(CURRENT_WEATHER_URL.format(city, API_KEY)).json()

    if response.get('cod') == 200:
        lat, lon = response['coord']['lat'], response['coord']['lon']
        forecast_response = requests.get(FORECAST_URL.format(lat, lon, API_KEY)).json()

        weather_data = {
            'city': city,
            'temperature': round(response['main']['temp'] - 273.15, 2),
            'description': response['weather'][0]['description'],
            'icon': response['weather'][0]['icon'],
        }

        serializer = WeatherSerializer(weather_data)

        forecast_data = forecast_response['daily'][:7]

        daily_forecasts = []
        for daily in forecast_data:
            day_timestamp = daily['dt']
            weekday = datetime.datetime.fromtimestamp(day_timestamp).strftime('%A')
            daily_forecasts.append({
                'day': weekday,
                'min_temp': round(daily['temp']['min'] - 273.15, 2),
                'max_temp': round(daily['temp']['max'] - 273.15, 2),
                'description': daily['weather'][0]['description'],
                'icon': daily['weather'][0]['icon'],
            })

        return render(request, 'api/weather.html',
                      {'weather_data2': serializer.data, 'daily_forecasts': daily_forecasts})
    else:
        return Response({"message": "Weather data not available for the specified city"},
                        status=status.HTTP_404_NOT_FOUND)


# Registration function
def register(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('current-weather')
    else:
        form = CustomUserCreationForm()
    return render(request, 'registration/register.html', {'form': form})


# Login function
def user_login(request):
    if request.method == 'POST':
        form = CustomAuthenticationForm(request, request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(request, username=username, password=password)
            if user:
                login(request, user)
                return redirect('current-weather')
    else:
        form = CustomAuthenticationForm()
    return render(request, 'registration/login.html', {'form': form})


def user_logout(request):
    logout(request)
    return redirect('current-weather')


# Unit tests
'''
# Generated by CodiumAI

# Dependencies:
# pip install pytest-mock
import pytest


class TestSearchWeather:

    #  Returns weather data and daily forecasts for a valid city
    def test_valid_city(self, mocker):
        # Mock the requests.get function to return a valid response
        mocker.patch('requests.get', return_value=MockResponse({
            'cod': 200,
            'coord': {'lat': 37.7749, 'lon': -122.4194},
            'main': {'temp': 300},
            'weather': [{'description': 'sunny', 'icon': '01d'}]
        }))

        # Call the search_weather function with a valid city
        response = self.client.get('/search_weather/?city=San Francisco')

        # Assert that the response contains the weather data and daily forecasts
        assert response.status_code == 200
        assert 'weather_data2' in response.context
        assert 'daily_forecasts' in response.context

    #  Returns 404 status and error message when API call fails
    def test_api_call_failure(self, mocker):
        # Mock the requests.get function to raise an exception
        mocker.patch('requests.get', side_effect=Exception('API call failed'))

        # Call the search_weather function
        response = self.client.get('/search_weather/?city=San Francisco')

        # Assert that the response contains the error message and has a 404 status code
        assert response.status_code == 404
        assert response.data['message'] == 'Weather data not available for the specified city'

        # Call the search_weather function
        response = self.client.get('/search_weather/?city=San Francisco')

        # Assert that the response contains the error message and has a 404 status code
        assert response.status_code == 404
        assert response.data['message'] == 'Weather data not available for the specified city'

    #  Returns correct temperature unit (Celsius) in weather data
    def test_temperature_unit_weather_data(self, mocker):
        # Mock the requests.get function to return a valid response
        mocker.patch('requests.get', return_value=MockResponse({
            'cod': 200,
            'coord': {'lat': 37.7749, 'lon': -122.4194},
            'main': {'temp': 300},
            'weather': [{'description': 'sunny', 'icon': '01d'}]
        }))

        # Call the search_weather function
        response = self.client.get('/search_weather/?city=San Francisco')

        # Assert that the temperatures in the daily forecasts are in Celsius
        assert response.status_code == 200
        for forecast in response.context['daily_forecasts']:
            assert forecast['min_temp'] == 26.85
            assert forecast['max_temp'] == 26.85
            
            

# Generated by CodiumAI

import pytest

class TestRegister:

    #  User submits valid registration form and is redirected to 'current-weather' page
    def test_valid_registration_form_redirect(self):
        # Create a mock request object with POST method
        request = RequestFactory().post('/register')
    
        # Create a mock form object with valid data
        form_data = {
            'username': 'testuser',
            'password1': 'testpassword',
            'password2': 'testpassword'
        }
        form = CustomUserCreationForm(data=form_data)
    
        # Call the register function with the mock request and form
        response = register(request)
    
        # Assert that the user is redirected to 'current-weather' page
        assert response.status_code == 302
        assert response.url == '/current-weather'

    #  User submits registration form with username that already exists in the database
    def test_existing_username_registration_form(self):
        # Create a mock request object with POST method
        request = RequestFactory().post('/register')
    
        # Create a mock form object with existing username
        form_data = {
            'username': 'piblo',
            'password1': '123456qweRT',
            'password2': '123456qweRT'
        }
        form = CustomUserCreationForm(data=form_data)
    
        # Call the register function with the mock request and form
        response = register(request)
    
        # Assert that the user sees an error message for existing username
        assert response.status_code == 200
        assert 'A user with that username already exists.' in response.context_data['form'].errors['username']

    #  User submits registration form with passwords that do not match
    def test_passwords_not_match_registration_form(self):
        # Create a mock request object with POST method
        request = RequestFactory().post('/register')
    
        # Create a mock form object with passwords that do not match
        form_data = {
            'username': 'testuser',
            'password1': '123456wqe',
            'password2': '123456asd'
        }
        form = CustomUserCreationForm(data=form_data)
    
        # Call the register function with the mock request and form
        response = register(request)
    
        # Assert that the user sees an error message for passwords not matching
        assert response.status_code == 200
        assert 'The two password fields didn’t match.' in response.context_data['form'].errors['__all__']

'''''
