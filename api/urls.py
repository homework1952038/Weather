from django.urls import path
from . import views

urlpatterns = [
    path('weather/current/', views.current_weather, name='current-weather'),
    path('weather/search/', views.search_weather, name='search-weather'),
    path('register/', views.register, name='register'),
    path('login/', views.user_login, name='login'),
    path('logout/', views.user_logout, name='logout'),
]